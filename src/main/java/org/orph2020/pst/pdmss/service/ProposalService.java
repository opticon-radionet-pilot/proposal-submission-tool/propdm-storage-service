package org.orph2020.pst.pdmss.service;
/*
 * Created on 16/03/2022 by Paul Harrison (paul.harrison@manchester.ac.uk).
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.smallrye.common.annotation.Blocking;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.ivoa.dm.proposal.prop.*;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

/**
 * business logic for the proposal DB
 */
@ApplicationScoped
public class ProposalService {

   private static final Logger LOGGER = Logger.getLogger("ProposalService");

   @Inject
   EntityManager em;

   @Inject
   ObjectMapper mapper;

   @Transactional
   public void initDB() {
      LOGGER.info("initializing Database");
      EmerlinExample ex = new EmerlinExample();
      em.persist(ex.getCycle());
      em.persist(ex.getProposal());
   }

   @Transactional
   public ProposalModel findAllProposals(String query) {
      List<ObservingProposal> observingProposals =
            em.createQuery("SELECT o FROM ObservingProposal o ORDER BY o.code", ObservingProposal.class)
            .getResultList();
      ProposalModel pm = new ProposalModel();

      for(ObservingProposal p : observingProposals) {
         p.walkCollections(); //IMPL force JPA loading
         pm.addContent(p);
      }
      pm.makeRefIDsUnique(); // TODO this should not be necessary
      return pm;
   }

   @Incoming("query") // from api-service in the form of a qlString
   @Outgoing("proposals") //return to api-service as a jsonString
   @Blocking
   public String proposalModelString(String query) throws JsonProcessingException {
      ProposalModel pm = findAllProposals(query);
      return mapper.writeValueAsString(pm);
   }
}
