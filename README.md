# propdm-service Project

This service provides a REST API to CRUD operations on the ProposalDM

Before testing this service it is necessary to clone https://github.com/ivoa/ProposalDM

and run

```shell
gradle publishToMavenLocal 
```

This project uses Quarkus [(more detail)](Developing.md), and to get started all that is needed
is that Docker is running and

```shell
quarkus dev
```

will run the server and allow for incremental development. 

To connect to the dev version of postgres that will run in Docker you need the following 

port : 64474
username : quarkus
password : quarkus


