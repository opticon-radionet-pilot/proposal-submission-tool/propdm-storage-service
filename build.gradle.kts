plugins {
    id("org.orph2020.pst.common-plugin")
}

version = "0.1.0-SNAPSHOT"

dependencies {

    implementation("io.quarkus:quarkus-hibernate-orm")
    implementation("io.quarkus:quarkus-rest-client-jackson")
    implementation("io.quarkus:quarkus-rest-client")
    implementation("io.quarkus:quarkus-hibernate-validator")
    implementation("io.quarkus:quarkus-rest-client-jaxb")
    implementation("io.quarkus:quarkus-jdbc-postgresql")
    implementation("io.quarkus:quarkus-arc")
    implementation("io.quarkiverse.helm:quarkus-helm:0.1.2")
    implementation("io.quarkus:quarkus-smallrye-reactive-messaging-kafka")
    testImplementation("io.rest-assured:rest-assured")
}
